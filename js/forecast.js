//https://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&appid=51e28eb0edf2201ad6b3e4121c280cb3

const key = "51e28eb0edf2201ad6b3e4121c280cb3";

const getForecast = async(city) => {
	const base = "https://api.openweathermap.org/data/2.5/forecast"
	const query = `?q=${city}&units=metric&appid=${key}`;
	const response = await fetch(base+query);
	// console.log(response);
	if(!response.ok)
		throw new Error("status code: "+response.status);
	const data = await response.json();
	// console.log(data);
	return data; 
}

// getForecast('mumbai')
// .then(data=>console.log(data))
// .catch(err=>console.warn(err));